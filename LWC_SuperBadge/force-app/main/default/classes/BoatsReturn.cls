public with sharing class BoatsReturn {
    @AuraEnabled(cacheable=true)
    public static List<Boat__c> getOneBoat(){
        String query = 'SELECT '
                     + 'Name, Description__c, Geolocation__Latitude__s, '
                     + 'Geolocation__Longitude__s, Picture__c, Contact__r.Name, '
                     + 'BoatType__c, BoatType__r.Name, Length__c, Price__c '
                     + 'FROM Boat__c LIMIT 1';
        return Database.query(query);
    }

}
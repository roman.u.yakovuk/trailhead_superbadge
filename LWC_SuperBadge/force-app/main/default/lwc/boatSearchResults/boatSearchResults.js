import { LightningElement, api, track, wire } from 'lwc';
import { APPLICATION_SCOPE, createMessageContext, MessageContext, publish, releaseMessageContext, subscribe, unsubscribe} from 'lightning/messageService';
import { updateRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import BOATMC from '@salesforce/messageChannel/BoatMessageChannel__c';
import getBoats from '@salesforce/apex/BoatDataService.getBoats';

const ERROR_TITLE = 'Error loading Boats';
const ERROR_VARIANT = 'error';
const COLLUMNS = [{
        label: 'Name',
        fieldName: 'Name',
        type: 'text',
        editable: true
    },
    {
        label: 'Length',
        fieldName: 'Length__c',
        type: 'number',
        editable: true
    },
    {
        label: 'Price',
        fieldName: 'Price__c',
        type: 'currency',
        editable: true
    },
    {
        label: 'Description',
        fieldName: 'Description__c',
        type: 'text area',
        editable: true
    }
]

export default class BoatSearchResults extends LightningElement {
    @track selectedBoatId;
    @track draftValues = [];
    @api boats = [];
    isLoading = true;
    @track columns = COLLUMNS;
    boatTypeId = '';

    @wire(MessageContext)
    messageContext;

    @wire(getBoats, {
        boatTypeId: '$boatTypeId',
    })

    wiredBoats({
        error,
        data
    }) {
        if (data) {
            this.boats = data;
            this.isLoading = false;
            console.log(this.columns);
        }
        if (error) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: ERROR_TITLE,
                    message: 'I can not upload records',
                    variant: ERROR_VARIANT
                })
            );
            this.isLoading = false;
        }
    }

    updateSelectedTile(event) {
        this.selectedBoatId = event.detail.boatId;
        this.sendMessageService(this.selectedBoatId)
    }
    
    @api searchBoats(boatTypeId) {
        this.boatTypeId = boatTypeId;
        this.notifyLoading(this.isLoading)
    }


    @api async refresh() {
        this.notifyLoading(true);
        return refreshApex(this.boats);
    }


    sendMessageService(boatId) {
        const message = {
            recordId: boatId,
            recordData: 'Current Boat Location'
        };
        publish(this.messageContext, BOATMC, message);
    }

    handleSave(event) {
        const recordInputs = event.detail.draftValues.slice().map(draft => {
            const fields = Object.assign({}, draft);
            return {
                fields
            };
        });

        const promises = recordInputs.map(recordInput =>
            updateRecord(recordInput)
        );
        Promise.all(promises)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Ship It!',
                        variant: 'success'
                    })
                );
                this.draftValues = [];
                this.refresh();
            }).catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error',
                        message: error,
                        variant: 'error'
                    })
                );
            })
            .finally(() => {});
    }

    notifyLoading(isLoading) {
        if (isLoading) {
            this.dispatchEvent(new CustomEvent('loading', {
                detail: isLoading
            }));
        } else {
            this.dispatchEvent(new CustomEvent('doneloading', {
                detail: isLoading
            }));
        }
    }
}
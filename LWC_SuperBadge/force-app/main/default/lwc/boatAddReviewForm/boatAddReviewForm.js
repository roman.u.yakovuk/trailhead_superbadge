import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const TOAST_SUCCESS_VARIANT = 'success';
const TOAST_TITLE = 'Review Created!';

export default class BoatAddReviewForm extends LightningElement {
    @track boatId;
    @track rating;
    
    @api get recordId() { 
        return this.recordId
    }
    set recordId(value) {
        this.boatId = value
    }
    
    handleRatingChanged(event) {
        this.rating = event.detail.rating;
     }
    
    handleSubmit(event) {
        event.preventDefault();
        const fields = event.detail.fields;
        fields.Boat__c = this.recordId;
        if(this.rating){
            fields.Rating__c = this.rating;
        }
        else{
            fields.Rating__c = 0;
        }
        this.template.querySelector('lightning-record-edit-form').submit(fields);
     }
    
    handleSuccess(event) {
        this.dispatchEvent(new ShowToastEvent({
            title: TOAST_TITLE,
            variant: TOAST_SUCCESS_VARIANT
        }));
        this.dispatchEvent(new CustomEvent('createreview'));
        this.handleReset();
      }
    
      handleReset() { 
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }
  }